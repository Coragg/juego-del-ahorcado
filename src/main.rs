use std::io::{self, Write};


fn main() {
    print!("Ingrese su nombre: ");
    io::stdout().flush().unwrap();
    let mut user_name: String = String::new();
    io::stdin().read_line(&mut user_name).unwrap();
    user_name = user_name.trim().to_string();
    println!("Bienvenido {} al juego del ahorcado:", user_name);
}
